import { ProductRequest } from './product-request';

export class CreateAssetRequest {
    maintenancePeriodicity: string;
    amountMaintenancePeriod: number;
    purchaseDate: Date;
    note: string;
    product: ProductRequest;

}
