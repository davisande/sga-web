import { ManufacturerResponse } from './manufacturer-response';

export class ProductResponse {
    id: number;
    name: string;
    type: string;
    manufacturer: ManufacturerResponse;

}