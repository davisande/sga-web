import { ProductResponse } from './product-response';

export class SearchAssetResponse {
    maintenancePeriodicity: string;
    amountMaintenancePeriod: number;
    purchaseDate: Date;
    lastMaintenanceDate: Date;
    note: string;
    creationDate: Date;
    updateDate: Date;
    product: ProductResponse;

}
