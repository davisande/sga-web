import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { CreateAssetRequest } from 'src/app/core/client/asset/dto/request/create-asset-request';
import { CreateAssetResponse } from 'src/app/core/client/asset/dto/response/create-asset-response';
import { environment } from 'src/environments/environment';
import { ManufacturerResponse } from './dto/response/manufacturer-response';
import { ProductResponse } from './dto/response/product-response';
import { SearchAssetResponse } from './dto/response/search-asset-response';

@Injectable({
  providedIn: 'root'
})
export class AssetClientService {

  constructor(private httpClient: HttpClient) { }

  createAsset(request: CreateAssetRequest): Observable<CreateAssetResponse> {
    const url = environment.apiGateway.url + environment.apiGateway.assetsPath;

    return this.httpClient.post<CreateAssetResponse>(url, request);
  }

  searchAllAssets(): Observable<SearchAssetResponse[]> {
    const url = environment.apiGateway.url + environment.apiGateway.assetsPath;

    return this.httpClient.get<SearchAssetResponse[]>(url);
  }

  searchAllManufecturers(): Observable<ManufacturerResponse[]> {
    const url = environment.apiGateway.url + environment.apiGateway.manufacturersPath;

    return this.httpClient.get<ManufacturerResponse[]>(url);
  }

  searchProductsByManufacturerId(manufacturerId: number): Observable<ProductResponse[]> {
    const url = environment.apiGateway.url + environment.apiGateway.manufacturersPath +
      '/' + manufacturerId + environment.apiGateway.productsPath;

    return this.httpClient.get<ProductResponse[]>(url);
  }

}
