import { TestBed } from '@angular/core/testing';

import { AssetClientService } from './asset-client.service';

describe('AssetClientService', () => {
  let service: AssetClientService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(AssetClientService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
