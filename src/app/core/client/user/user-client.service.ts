import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { UserAuthenticationRequest } from './dto/user-authentication-request';
import { UserAuthenticationRespose } from './dto/user-authentication-respose';
import { TokenValidationResponse } from './dto/token-validation-response';

@Injectable({
  providedIn: 'root'
})
export class UserClientService {

  constructor(private httpClient: HttpClient) { }

  authenticateUser(request: UserAuthenticationRequest): Observable<UserAuthenticationRespose> {
    const url = environment.apiGateway.url + environment.apiGateway.userAuthenticatePath;

    return this.httpClient.post<UserAuthenticationRespose>(url, request);
  }

  validateToken(): Observable<TokenValidationResponse> {
    const url = environment.apiGateway.url + environment.apiGateway.validateToken;

    return this.httpClient.get<TokenValidationResponse>(url);
  }

}
