import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from "rxjs";

@Injectable()
export class AuthInterceptor implements HttpInterceptor {

    constructor() { }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        if (request.url.includes('/users/authenticate')) {
            return next.handle(request);
        }

        request = request.clone({
            headers: request.headers.set('Authorization', sessionStorage.token)
        });

        return next.handle(request);
    }
}