import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LoginComponent } from './login/login.component';
import { AssetFormComponent } from './asset/asset-form/asset-form.component';
import { AuthGuard } from './core/guard/auth.guard';
import { AssetListComponent } from './asset/asset-list/asset-list.component';


const routes: Routes = [
  { path: '', redirectTo: '/login', pathMatch: 'full' },
  { path: 'login', component: LoginComponent },
  { path: 'assets', component: AssetListComponent, canActivate: [AuthGuard] },
  { path: 'assets/add', component: AssetFormComponent, canActivate: [AuthGuard] }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
