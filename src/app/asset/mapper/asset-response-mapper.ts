import { Injectable } from '@angular/core';
import { CreateAssetResponse } from 'src/app/core/client/asset/dto/response/create-asset-response';
import { ManufacturerResponse } from 'src/app/core/client/asset/dto/response/manufacturer-response';
import { ProductResponse } from 'src/app/core/client/asset/dto/response/product-response';
import { Asset } from '../dto/asset';
import { Manufacturer } from '../dto/manufacturer';
import { Product } from '../dto/product';
import { SearchAssetResponse } from 'src/app/core/client/asset/dto/response/search-asset-response';

@Injectable({
    providedIn: 'root'
})
export class AssetResponseMapper {

    fromCreateAssetResponse(createAssetResponse: CreateAssetResponse): Asset {
        const asset = new Asset();
        asset.maintenancePeriodicity = createAssetResponse.maintenancePeriodicity;
        asset.amountMaintenancePeriod = createAssetResponse.amountMaintenancePeriod;
        asset.lastMaintenanceDate = createAssetResponse.lastMaintenanceDate;
        asset.purchaseDate = createAssetResponse.purchaseDate;
        asset.note = createAssetResponse.note;
        asset.creationDate = createAssetResponse.creationDate;
        asset.updateDate = createAssetResponse.updateDate;
        asset.product = this.mapperProductResponse(createAssetResponse.product);

        return asset;
    }

    fromSearchAssetResponseList(searchAssetResponseList: SearchAssetResponse[]): Asset[] {
        const assets = new Array<Asset>();

        searchAssetResponseList.forEach(
            searchAssetResponse => assets.push(searchAssetResponse)
        );

        return assets;
    }

    fromSearchAssetResponse(searchAssetResponse: SearchAssetResponse): Asset {
        const asset = new Asset();
        asset.maintenancePeriodicity = searchAssetResponse.maintenancePeriodicity;
        asset.amountMaintenancePeriod = searchAssetResponse.amountMaintenancePeriod;
        asset.lastMaintenanceDate = searchAssetResponse.lastMaintenanceDate;
        asset.purchaseDate = searchAssetResponse.purchaseDate;
        asset.note = searchAssetResponse.note;
        asset.creationDate = searchAssetResponse.creationDate;
        asset.updateDate = searchAssetResponse.updateDate;
        asset.product = this.mapperProductResponse(searchAssetResponse.product);

        return asset;
    }

    manufacturersFromManufacturersResponse(
        manufacturersResponse: ManufacturerResponse[]): Manufacturer[] {
        const manufacturers = new Array<Manufacturer>();

        manufacturersResponse.forEach(
            manufacturerResponse => manufacturers.push(
                this.mapperManufacturerResponse(manufacturerResponse))
        );

        return manufacturers;
    }

    productsFromProductsResponse(productsResponse: ProductResponse[]): Product[] {
        const products = new Array<ProductResponse>();

        productsResponse.forEach(
            productResponse => products.push(this.mapperProductResponse(productResponse))
        );

        return products;
    }

    private mapperProductResponse(productResponse: ProductResponse): Product {
        const product = new Product();
        product.id = productResponse.id;
        product.name = productResponse.name;
        product.type = productResponse.type;

        if (productResponse.manufacturer != null){
            product.manufacturer = this.mapperManufacturerResponse(productResponse.manufacturer);
        }

        return product;
    }

    private mapperManufacturerResponse(manufacturerResponse: ManufacturerResponse): Manufacturer {
        const manufacturer = new Manufacturer();
        manufacturer.id = manufacturerResponse.id;
        manufacturer.name = manufacturerResponse.name;

        return manufacturer;
    }

}
