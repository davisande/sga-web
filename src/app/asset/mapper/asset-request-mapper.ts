import { Injectable } from '@angular/core';
import { CreateAssetRequest } from 'src/app/core/client/asset/dto/request/create-asset-request';
import { ProductRequest } from 'src/app/core/client/asset/dto/request/product-request';
import { Asset } from '../dto/asset';
import { Product } from '../dto/product';

@Injectable({
    providedIn: 'root'
})
export class AssetRequestMapper {

    toCreateAssetResquest(asset: Asset): CreateAssetRequest {
        const createAssetRequest = new CreateAssetRequest();
        createAssetRequest.maintenancePeriodicity = asset.maintenancePeriodicity;
        createAssetRequest.amountMaintenancePeriod = asset.amountMaintenancePeriod;
        createAssetRequest.purchaseDate = asset.purchaseDate;
        createAssetRequest.note = asset.note;
        createAssetRequest.product = this.mapperProduct(asset.product);

        return createAssetRequest;
    }

    private mapperProduct(product: Product): ProductRequest {
        const productRequest = new ProductRequest();
        productRequest.id = product.id;

        return productRequest;
    }

}
