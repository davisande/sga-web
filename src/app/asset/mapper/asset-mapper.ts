import { Injectable } from '@angular/core';
import { Asset } from '../dto/asset';
import { Product } from '../dto/product';

@Injectable({
    providedIn: 'root'
})
export class AssetMapper {

    fromFormObject(formObject): Asset {
        const asset = new Asset();
        asset.maintenancePeriodicity = formObject.maintenancePeriodicity;
        asset.amountMaintenancePeriod = formObject.amountMaintenancePeriod;
        asset.lastMaintenanceDate = formObject.lastMaintenanceDate;
        asset.purchaseDate = this.convertShortDateToIsoDateTime(formObject.purchaseDate);
        asset.note = formObject.note;
        asset.creationDate = formObject.creationDate;
        asset.updateDate = formObject.updateDate;

        const product = new Product();
        product.id = formObject.productId;
        asset.product = product;

        return asset;
    }

    private convertShortDateToIsoDateTime(shortDate: string): Date {
        const splitedShortDate = shortDate.split('/');
        const isoDate = `${splitedShortDate[2]}-${splitedShortDate[1]}-${splitedShortDate[0]}`;
        const isoTime = 'T00:00:00Z';

        return new Date(`${isoDate}${isoTime}`);
    }
}
