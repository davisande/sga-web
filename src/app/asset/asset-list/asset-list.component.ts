import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AssetClientService } from 'src/app/core/client/asset/asset-client.service';
import { Asset } from '../dto/asset';
import { AssetResponseMapper } from '../mapper/asset-response-mapper';

@Component({
  selector: 'app-asset-list',
  templateUrl: './asset-list.component.html',
  styleUrls: ['./asset-list.component.css']
})
export class AssetListComponent implements OnInit {
  assets: Asset[];

  constructor(
    private assetClientService: AssetClientService,
    private assetResponseMapper: AssetResponseMapper,
    private router: Router,
  ) { }

  ngOnInit(): void {
    this.loadAssets();
  }

  private loadAssets() {
    this.assetClientService.searchAllAssets().subscribe(
      searchAssetResponse =>
        this.assets = this.assetResponseMapper.fromSearchAssetResponseList(searchAssetResponse),
      sbError => console.log(sbError)
    );
  }

  backToAssetsAdd() {
    this.router.navigate(['/assets/add']);
  }

}
