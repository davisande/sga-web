import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AssetFormComponent } from './asset-form/asset-form.component';
import { ReactiveFormsModule } from '@angular/forms';
import { AssetListComponent } from './asset-list/asset-list.component';



@NgModule({
  declarations: [AssetFormComponent, AssetListComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule
  ]
})
export class AssetModule { }
