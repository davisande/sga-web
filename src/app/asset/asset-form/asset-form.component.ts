import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { Manufacturer } from '../dto/manufacturer';
import { Product } from '../dto/product';
import { AssetMapper } from '../mapper/asset-mapper';
import { AssetFormService } from './asset-form.service';

@Component({
  selector: 'app-asset-form',
  templateUrl: './asset-form.component.html',
  styleUrls: ['./asset-form.component.css']
})
export class AssetFormComponent implements OnInit {
  assetForm: FormGroup;
  manufacturers: Manufacturer[];
  products: Product[];
  maintenancePeriodicities: any;

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private assetFormService: AssetFormService,
    private assetMapper: AssetMapper
  ) { }

  ngOnInit(): void {
    this.buildLoginForm();
    this.loadManufacturers();
    this.loadMaintenancePeriodicities();
  }

  private buildLoginForm() {
    this.assetForm = this.formBuilder.group({
      manutacturerId: [''],
      productId: [''],
      maintenancePeriodicity: [''],
      amountMaintenancePeriod: [''],
      purchaseDate: [''],
      note: ['']
    });
  }

  private loadManufacturers() {
    this.assetFormService.searchAllManufacturers()
      .subscribe(
        manufacturers => this.manufacturers = manufacturers,
        sbError => console.log(sbError)
      )
  }

  private loadMaintenancePeriodicities() {
    this.maintenancePeriodicities = [
      { acronyms: 'DA', name: 'Diária' },
      { acronyms: 'WE', name: 'Semanal' },
      { acronyms: 'YE', name: 'Anual' }
    ];
  }

  loadProductsByManufacturer() {
    const manufacturerId = this.assetForm.get('manutacturerId').value;

    this.assetFormService
      .searchProductsByManufacturerId(manufacturerId)
      .subscribe(
        products => this.products = products,
        sbError => console.log(sbError)
      );
  }

  createAsset() {
    const asset = this.assetMapper.fromFormObject(this.assetForm.value);

    this.assetFormService.createAsset(asset)
      .subscribe(
        createAssetResponse => {
          this.assetForm.reset();
          alert('Ativo salvo com sucesso!');
        },
        sbError => console.log(sbError)
      );
  }

  backToLogin() {
    this.router.navigate(['/login']);
  }

  forwardToAssetList() {
    this.router.navigate(['/assets']);
  }

}
