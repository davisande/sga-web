import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { AssetClientService } from 'src/app/core/client/asset/asset-client.service';
import { CreateAssetResponse } from 'src/app/core/client/asset/dto/response/create-asset-response';
import { Asset } from '../dto/asset';
import { Manufacturer } from '../dto/manufacturer';
import { Product } from '../dto/product';
import { AssetRequestMapper } from '../mapper/asset-request-mapper';
import { AssetResponseMapper } from '../mapper/asset-response-mapper';

@Injectable({
  providedIn: 'root'
})
export class AssetFormService {

  constructor(
    private assetClientService: AssetClientService,
    private assetResponseMapper: AssetResponseMapper,
    private assetRequestMapper: AssetRequestMapper
  ) { }

  searchAllManufacturers(): Observable<Manufacturer[]> {
    return this.assetClientService.searchAllManufecturers()
      .pipe(map(manufacturersResponse => this.assetResponseMapper
        .manufacturersFromManufacturersResponse(manufacturersResponse)))
  }

  searchProductsByManufacturerId(manufacturerId: number): Observable<Product[]> {
    return this.assetClientService.searchProductsByManufacturerId(manufacturerId)
      .pipe(map(productsResponse => this.assetResponseMapper
        .productsFromProductsResponse(productsResponse)));
  }

  createAsset(asset: Asset): Observable<CreateAssetResponse> {
    const createAssetRequest = this.assetRequestMapper.toCreateAssetResquest(asset);

    return this.assetClientService.createAsset(createAssetRequest)
      .pipe(map(createAssetResponse => this.assetResponseMapper
        .fromCreateAssetResponse(createAssetResponse)));
  }

}
