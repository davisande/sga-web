import { TestBed } from '@angular/core/testing';

import { AssetFormService } from './asset-form.service';

describe('AssetFormService', () => {
  let service: AssetFormService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(AssetFormService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
