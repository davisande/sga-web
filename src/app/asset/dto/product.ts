import { Manufacturer } from './manufacturer';

export class Product {
    id: number;
    name: string;
    type: string;
    manufacturer: Manufacturer;

}