import { Product } from './product';

export class Asset {
    maintenancePeriodicity: string;
    amountMaintenancePeriod: number;
    purchaseDate: Date;
    lastMaintenanceDate: Date;
    note: string;
    creationDate: Date;
    updateDate: Date;
    product: Product;

}
