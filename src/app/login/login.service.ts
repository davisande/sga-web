import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { UserClientService } from '../core/client/user/user-client.service';
import { Login } from './dto/login';
import { LoginMapper } from './mapper/login-mapper';

@Injectable({
  providedIn: 'root'
})
export class LoginService {
  private tokenPrefix = 'Bearer ';

  constructor(
    private loginMapper: LoginMapper,
    private userClientService: UserClientService,
    private router: Router
  ) { }

  authenticateUser(login: Login): Observable<string> {
    const request = this.loginMapper.toUserAutenticationRequest(login);

    return this.userClientService.authenticateUser(request)
      .pipe(map(resp => this.tokenPrefix + resp.token));
  }

  validateToken(): Observable<boolean> {
    const token = sessionStorage.getItem('token');
    if (token == null) {
      alert("Acesso não autorizado. Por favor, faça o login.")
      this.router.navigate(['/login'])
    }

    return this.userClientService.validateToken().pipe(map(resp => resp.valid));
  }

}
