import { Login } from '../dto/login';
import { UserAuthenticationRequest } from 'src/app/core/client/user/dto/user-authentication-request';
import { Injectable } from '@angular/core';
import { UserAuthenticationRespose } from 'src/app/core/client/user/dto/user-authentication-respose';

@Injectable({
    providedIn: 'root'
})
export class LoginMapper {

    constructor() { }

    toUserAutenticationRequest(login: Login): UserAuthenticationRequest {
        const request = new UserAuthenticationRequest();
        request.username = login.username;
        request.password = login.password;

        return request;
    }

}
