import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { LoginService } from './login.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit, OnDestroy {
  loginForm: FormGroup;
  private subscriptionAuth: Subscription;

  constructor(
    private formBuilder: FormBuilder,
    private loginService: LoginService,
    private router: Router
  ) { }

  ngOnInit(): void {
    sessionStorage.clear();
    this.buildLoginForm();
  }

  private buildLoginForm() {
    this.loginForm = this.formBuilder.group({
      username: [''],
      password: ['']
    });
  }

  performLogin() {
    const fieldsForm = this.loginForm.value;

    this.subscriptionAuth = this.loginService.authenticateUser(fieldsForm)
      .subscribe(token => {
        sessionStorage.setItem('token', token);
        this.router.navigate(['/assets/add']);
      },
      errorSub => console.log(errorSub));
  }

  ngOnDestroy() {
    this.subscriptionAuth.unsubscribe();
  }

}
