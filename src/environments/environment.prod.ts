export const environment = {
  production: true,
  apiGateway: {
    url: 'http://localhost:8083',
    userAuthenticatePath: '/users/authenticate',
    validateToken: '/users/validate-token', 
    assetsPath: '/assets',
    manufacturersPath: '/manufacturers',
    productsPath: '/products'
  }
};
